package com.webster;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;
import android.content.res.TypedArray;

import com.webster.db.DBHelper;
import com.webster.db.PlantDataSource;
import  com.webster.model.Photo;

public class PhotoListActivity extends Activity implements View.OnClickListener {
    private ListView lv1;
    public static final String TAG = "PhotoListActivity";
    private GridView gridView;
    private GridViewAdapter customGridAdapter;

    private static final int ID_PhotoActivity = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_photolist);


        gridView = (GridView) findViewById(R.id.gridView);
        customGridAdapter = new GridViewAdapter(this, R.layout.row_grid, getData());
        gridView.setAdapter(customGridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(PhotoListActivity.this, position + "#Selected",
                        Toast.LENGTH_SHORT).show();
            }

        });

        Bundle b = getIntent().getExtras();
        lv1 = (ListView) findViewById(R.id.photos); // Получим идентификатор ListView
        String[]ar=null;
        ar=getPhotosByPlantId(b.getLong("plantId"));
        Log.d("idddddd",b.getLong("plantId")+" id");
        Log.d("iddddddlen",ar.length+"");
        if(ar.length!=0){
            lv1.setAdapter(customGridAdapter);
//        lv1.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,ar )); //устанавливаем массив в ListView
//            lv1.setTextFilterEnabled(true);



        //Обрабатываем щелчки на элементах ListView:
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                //Позиция элемента, по которому щелкнули
                String itemname = new Integer(position + 1).toString();
                Intent intent = new Intent();
                intent.setClass(PhotoListActivity.this, AddPhotoActivity.class);
                Bundle b = new Bundle();
                Photo selectedPhoto=getPhoto(position + 1);
                b.putLong("plantId", selectedPhoto.getId()); //defStrID содержит строку, которую отправим через itemname в другое Activity
                b.putString("plantName",selectedPhoto.getDescription() );
                intent.putExtras(b);
                startActivityForResult(intent, ID_PhotoActivity);
            }
        });
        }


        else
        {
            Toast.makeText(PhotoListActivity.this, "пууусто",
                    Toast.LENGTH_SHORT).show();
        }
        Button upload = (Button) findViewById(R.id.upload);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = getIntent().getExtras();
                long plantId = b.getLong("plantId");
                Log.d("plantId PhotoList",b.getLong("plantId")+"");
                String plantName = b.getString("plantName");
                Log.d("plant_name",b.getString("plantName"));
                b.putLong("plantId", plantId); //defStrID содержит строку, которую отправим через itemname в другое Activity
                b.putString("plantName", plantName);
                Intent intent = new Intent(getApplicationContext(), AddPhotoActivity.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });


        Button VK = (Button) findViewById(R.id.VKbutton);
        VK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myprof = new Intent(PhotoListActivity.this,VKAuthorizationActivity.class);
                startActivity(myprof);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case ID_PhotoActivity:{
                Log.d(TAG, data.toString());
                Log.d(TAG, "" + data.getLongExtra("plantId", 0));
                break;
            }
            default: ;
        }
    }

    public Photo getPhoto(long id) {
        DBHelper dbHelper = new DBHelper(this);
        PlantDataSource ds = new PlantDataSource(this);
        ds.open();
        Photo photo = ds.getPhoto(id);
        Log.d("dash", photo.toString());
        ds.close();
        return photo;
    }
    public String[] getPhotosByPlantId(long id) {

        DBHelper dbHelper = new DBHelper(this);
        PlantDataSource ds = new PlantDataSource(this);
        ds.open();
        List<Photo> photos = ds.getPhotosByPlantID(id);
        Log.d("gettedById", photos.toString());
        ds.close();

        String result1[] = new String[photos.size()];
        int i = 0;
        for (Photo photo : photos) {
            result1[i] = photo.getDescription();
            i++;
        }
        return result1;

    }
    private ArrayList<ImageItem> getData()  {
        ArrayList<ImageItem> imageItems = new ArrayList<>();

            // retrieve String drawable array
            DBHelper dbHelper = new DBHelper(this);
            PlantDataSource ds = new PlantDataSource(this);
            ds.open();
            // imageItems=ds.getPhotosByPlantID(1);
            TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
            Bundle bundle = getIntent().getExtras();
            String itemname = "image_" + "дальше идут айдишник и имя растения,которые передались из предыдущего активити   " + bundle.getLong("plantId") + "   " + bundle.getString("plantName"); //получаем строку и формируем имя ресурса
            gridView = (GridView) findViewById(R.id.gridView); // Получим идентификатор ListView


            for (int i = 5; i < 6; i++) {
//            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),
//                    imgs.getResourceId(i, -1));
//            imageItems.add(new ImageItem(bitmap, itemname));
                List<Photo> photos = ds.getPhotosByPlantID(bundle.getLong("plantId"));
                if (!photos.isEmpty()) {

                    Log.d(TAG, "path to photo = " + ds.getPhotosByPlantID(bundle.getLong("plantId")).get(0).getPath());
                    Log.d(TAG, "path = " + getContentResolver());
                    Bitmap bitmap;

                    //bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), ds.getPhotosByPlantID(bundle.getLong("plantId")).get(0).getPath());
                    bitmap = BitmapFactory.decodeFile(ds.getPhotosByPlantID(bundle.getLong("plantId")).get(0).getPath().toString());

                    imgs.getResourceId(i, -1);
                    imageItems.add(new ImageItem(bitmap, itemname));



                }
            }


            return imageItems;

        }


    @Override
    public void onClick(View v) {

    }
}