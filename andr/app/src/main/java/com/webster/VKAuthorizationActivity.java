package com.webster;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.net.URLEncoder;

public class VKAuthorizationActivity extends Activity {

    private static final String TAG = "VKAuthorizationActivity";
    private static final String VK_CLIENT_ID = "4840832";
    private String ACCESS_TOKEN="ACCESS_TOKEN";
    private String UID="UID";

   // private String urlpost="";
    private class ViewHolder {
        WebView webView;
        ProgressBar pb;
    }

    private ViewHolder vh = new ViewHolder();

    private CookieManager cookieManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vkauthorization);

        initializeView();

        vh.webView.getSettings().setJavaScriptEnabled(true);
        vh.webView.setVerticalScrollBarEnabled(true);
        vh.webView.setHorizontalScrollBarEnabled(true);
        vh.webView.setWebViewClient(new VkWebViewClient());

        vh.webView.setVisibility(View.VISIBLE);
        vh.webView.loadUrl(createURL());
        Intent intent=getIntent();
        String Token=intent.getStringExtra("ACCESS_TOKEN");
        String User=intent.getStringExtra("UID");
       // Log.d("!!!!!!!!!!",Token+"  "+ User+" !!!");
        //vh.webView.loadUrl("https://api.vk.com/method/wall.post?owner_id="+User+"&message=Загружено через Plants&attachments=photo16303414_249428237&access_token="+Token);

        vh.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                vh.pb.setProgress(progress);
                Log.d("TAG", String.valueOf(progress));
                super.onProgressChanged(view, progress);
            }
        });

        cookieManager = CookieManager.getInstance();

        vh.pb = (ProgressBar) findViewById (R.id.pb);
        vh.pb.setMax(100);
    }

    private void initializeView() {
        vh.webView = (WebView) findViewById(R.id.web_view);
    }

    //TODO replace method encode
    private String createURL() {
        String url = "https://oauth.vk.com/authorize?client_id=" + VK_CLIENT_ID +
                "&scope=wall,offline&redirect_uri=" + URLEncoder.encode("https://oauth.vk.com/blank.html") +
                "&display=mobile&v=5.29" + "&response_type=token";
        //String url = "https://oauth.vk.com/authorize?client_id=4829914&scope=wall,offline&redirect_uri=https://oauth.vk.com/blank.html&display=mobile&v=5.29&response_type=token";
        //String url = "https://vk.com";
        return url;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class VkWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.d(TAG, url);
            checkingUrlError(url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (url.startsWith("http://oauth.vk.com/blank.html") || url.startsWith("https://oauth.vk.com/blank.html"))
                buildDate(url);
        }

        private void buildDate(String url) {
            checkingUrlError(url);
            Log.d("URL", url);
            String[] auth = parseRedirectUrl(url);
            Log.d("auth[0]", auth[0]);
            Log.d("auth[1]", auth[1]);
            //Token=auth[0];
            //User=auth[1];

            Intent intent = new Intent();
            intent.putExtra(ACCESS_TOKEN, auth[0]);
            intent.putExtra(UID, auth[1]);

            Log.d("TAG", "setResult");
            cookieManager.removeAllCookie();
            setResult(Activity.RESULT_OK, intent);
            vh.webView.loadUrl("https://api.vk.com/method/wall.post?owner_id="+auth[1]+"&message=Загружено через Plants&attachments=photo16303414_249428237&access_token="+auth[0]);
            Toast.makeText(getApplicationContext(), "Запись опубликована", Toast.LENGTH_LONG).show();


            finish();
        }

        public String[] parseRedirectUrl(String url) {
            String[] auth = new String[2];
            int start = url.indexOf("=") + 1;
            int end = url.indexOf("&");
            auth[0] = url.substring(start, end);
            start = url.indexOf("=", url.indexOf("=", end) + 1) + 1;
            end = url.length();
            auth[1] = url.substring(start, end);
            return auth;
        }

        public void checkingUrlError(String url) {
            if (url == null || url.contains("error")) {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }
}
