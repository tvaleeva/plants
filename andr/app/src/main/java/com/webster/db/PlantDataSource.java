package com.webster.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.webster.model.Photo;
import com.webster.model.Plant;
import com.webster.ImageItem;

import java.util.ArrayList;
import java.util.List;

public class PlantDataSource {
    private static String TAG = "PlantDataSource";
    // Database fields
    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private String[] allPlantColumns = {DBHelper.ID, DBHelper.NAME};
    private String[] allPhotoColumns = {DBHelper.PHOTO_ID, DBHelper.PLANT_ID, DBHelper.PATH, DBHelper.PHOTO_DESCRIPTION};

    public PlantDataSource(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long createPlant(String name) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.NAME, name);
        long insertId = database.insert(DBHelper.TABLE_PLANTS, null,
                values);
        Cursor cursor = database.query(DBHelper.TABLE_PLANTS,
                null, DBHelper.ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Long id = Long.parseLong(cursor.getString(cursor.getColumnIndex(DBHelper.ID)));
        Plant plant = new Plant(id, name);
        Log.d(TAG, "created " + id);
        String names[] = cursor.getColumnNames();
        for (int i = 0; i < names.length; i++) {
            Log.d(TAG, names[i]);
        }
        cursor.close();
        return id;
    }

    public Plant getPlant(long id) {
        Plant plant = null;
        Cursor cursor = database.query(dbHelper.TABLE_PLANTS,
                allPlantColumns, DBHelper.ID + " = " + id, null, null, null, null);
        cursor.moveToFirst();
        plant = cursorToPlant(cursor);
        // make sure to close the cursor
        cursor.close();
        return plant;
    }

    public Photo getPhoto(long id) {
        Photo photo = null;
        Cursor cursor = database.query(dbHelper.TABLE_PLANTS,
                allPlantColumns, DBHelper.ID + " = " + id, null, null, null, null);
        cursor.moveToFirst();
        photo = cursorToPhoto(cursor);
        // make sure to close the cursor
        cursor.close();
        return photo;
    }

    public long createPhoto(long plantId, String descr, Uri path) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.PHOTO_DESCRIPTION, descr);
        values.put(DBHelper.PLANT_ID, plantId);
        String[] pathParts = path.toString().split("/");
        values.put(DBHelper.PATH, pathParts[pathParts.length - 1]);
        long insertId = database.insert(DBHelper.TABLE_PHOTO, null,
                values);
        Log.d(TAG, "insert id = " + insertId);
        Cursor cursor = database.query(DBHelper.TABLE_PHOTO,
                null, DBHelper.PHOTO_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Log.d(TAG, "cursor length = " + cursor.getColumnCount());
        Long id = Long.parseLong(cursor.getString(cursor.getColumnIndex(DBHelper.PHOTO_ID)));
        Photo photo = new Photo(id, plantId, path, descr);
        Log.d(TAG, "photo created " + id);
        String names[] = cursor.getColumnNames();
        for (int i = 0; i < names.length; i++) {
            Log.d(TAG, names[i]);
        }
        cursor.close();
        return id;
    }

    public void deletePlant(Plant Plant) {
        long id = Plant.getId();
        System.out.println("Plant deleted with id: " + id);
        database.delete(dbHelper.TABLE_PLANTS, dbHelper.ID
                + " = " + id, null);
    }

    public List<Plant> getAllPlants() {
        List<Plant> plants = new ArrayList<Plant>();

        Cursor cursor = database.query(dbHelper.TABLE_PLANTS,
                allPlantColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Plant Plant = cursorToPlant(cursor);
            plants.add(Plant);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return plants;
    }

    public List<Photo> getAllPhotos() {
        List<Photo> photos = new ArrayList<Photo>();

        Cursor cursor = database.query(dbHelper.TABLE_PHOTO,
                allPhotoColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Photo photo = cursorToPhoto(cursor);
            photos.add(photo);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return photos;
    }

    public List<Photo> getPhotosByPlantID(long id) {
        List<Photo> photos = new ArrayList<Photo>();
        Log.d("allPhoto", getAllPhotos().toString());
        Log.d("allPhotoPlantId", id+"");

        Cursor cursor = database.query(dbHelper.TABLE_PHOTO,
                allPhotoColumns, DBHelper.PLANT_ID + " = " + id, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Photo photo = cursorToPhoto(cursor);
            photos.add(photo);
            cursor.moveToNext();
        }

        cursor.close();
        return photos;
    }


    private Plant cursorToPlant(Cursor cursor) {
        Plant plant = new Plant(cursor.getLong(0), cursor.getString(1));
        return plant;
    }

    private Photo cursorToPhoto(Cursor cursor) {
        long photo_id = cursor.getColumnIndex("PHOTO_ID");
        long plant_id = cursor.getColumnIndex("PLANT_ID");
        int description = cursor.getColumnIndex("PHOTO_DESCRIPTION");

        Uri uri = Uri.parse(cursor.getString(2));
        Photo photo = new Photo(photo_id, plant_id, uri, cursor.getString(3));
        return photo;
    }


}
