package com.webster.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by 007 on 09.03.2015.
 */
public class DBHelper extends SQLiteOpenHelper {
    final String LOG_TAG = "myLogs";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public static final String TABLE_PLANTS = "PLANTS";
    public static final String ID = "_id";
    public static final String NAME = "NAME";

    public static final String TABLE_PHOTO = "PHOTO";
    public static final String PHOTO_ID = "PHOTO_ID";
    public static final String PLANT_ID="PLANT_ID";
    public static final String PATH="PATH";
    public static final String PHOTO_DESCRIPTION="PHOTO_DESCRIPTION";

    public static final String DATABASE_NAME="myDB";
    public static final int DATABASE_VERSION = 1;


    public static final String DATABASE_CREATE = "create table " + TABLE_PLANTS + "(" +
            ID + " integer primary key autoincrement, " +
            NAME + " text not null); ";



    public static final String DATABASE_CREATE_TABLE_PHOTO =
            "create table " + TABLE_PHOTO +"("+
            PHOTO_ID +" integer primary key autoincrement, "+
            PLANT_ID +" integer not null, "+
            PHOTO_DESCRIPTION +" text, "+
            PATH +" text);";

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "--- onCreate database ---");
        db.execSQL(DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE_TABLE_PHOTO);
      /*  db.execSQL("CREATE TABLE plants (" +
                "ID integer primary key autoincrement," +
                "name text," +
                "descr text" +
                ");" +
                "CREATE TABLE photo (" +
                "plant_ID integer," +
                "path text," +
                "FOREIGN KEY(plant_ID) REFERENCES plants(ID)" +
                ");");*/
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
