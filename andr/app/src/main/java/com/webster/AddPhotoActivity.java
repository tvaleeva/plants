package com.webster;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.webster.db.DBHelper;
import com.webster.db.PlantDataSource;
import com.webster.model.Photo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;


public class AddPhotoActivity extends Activity {
    private static final String TAG = "AddPhotoActivity";
    static Button myButton;
static ImageView myImageView;
    static final int GALLERY_REQUEST = 1;
    static  Uri uri = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);


         myButton = (Button)findViewById(R.id.getPhoto);
        myImageView = (ImageView)findViewById(R.id.photo);

        myButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });
        Button tomain = (Button) findViewById(R.id.toMain);
        tomain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        Button addPhoto = (Button) findViewById(R.id.addPhoto);
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Bundle bundle = getIntent().getExtras();
                long plantId = bundle.getLong("plantId");
                String plantName = bundle.getString("plantName");
                EditText photoDescr = (EditText)findViewById(R.id.photoDescr);
                String descr=photoDescr.getText().toString();
               long id= addPhoto(plantId,descr,uri);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
*/
                Bundle bundle = getIntent().getExtras();
                long plantId = bundle.getLong("plantId");
                String plantName = bundle.getString("plantName");
                EditText photoDescr = (EditText)findViewById(R.id.photoDescr);
                String descr=photoDescr.getText().toString();
                long id= addPhoto(plantId,descr,uri);

                Intent intent2 = new Intent();
                intent2.putExtra("plantId", plantId);
                intent2.putExtra("plantName", plantName);
                setResult(RESULT_OK, intent2);
                finish();
            }
        });
    }

    public long addPhoto(long plantId,String descr,Uri path) {
        Long id;
        DBHelper dbHelper = new DBHelper(this);
        PlantDataSource ds = new PlantDataSource(this);
        ds.open();
       //сделать проверку на нулл if(path!=null) {
            id = ds.createPhoto(plantId, descr, path);
        //}
        Log.d("photos", ds.getAllPhotos().toString());
        ds.close();
        return id;
    }
    public void setUri(String uri){
        this.uri=Uri.parse(uri);
    }
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
            super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

            Bitmap galleryPic = null;

            switch(requestCode) {
                case GALLERY_REQUEST:
                    if(resultCode == RESULT_OK){
                        Uri selectedImage = imageReturnedIntent.getData();

                        //selectedImage.getPath()
                        String selectedImagePath = ImageFilePath.getPath(getApplicationContext(), selectedImage);
                        setUri(selectedImagePath);

                            //galleryPic = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                            galleryPic = BitmapFactory.decodeFile( selectedImagePath);
                        Log.d(TAG, "image = " + selectedImage);

                        myImageView.setImageBitmap(galleryPic);
                    }
            }
        }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





}
