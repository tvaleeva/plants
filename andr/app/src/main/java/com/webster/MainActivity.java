package com.webster;

import android.app.Activity;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.Switch;

import com.webster.model.Photo;
import com.webster.model.Plant;

import com.webster.db.DBHelper;
import com.webster.db.PlantDataSource;

import java.util.List;


public class MainActivity extends Activity implements OnClickListener {
    private static final String LOG_TAG = "myLogs";
    private ListView lv1;
    private static final int STOPSPLASH = 0;
    private static final long SPLASHTIME = 1800;
    private static final int ID_PhotoListActivity = 100;

    private ImageView splash;
    Button btnActTwo;
    //массив разделов
    private String lv_arr[] = {"00. раст 0.", "01. раст 1.", "02. раст2"};

    private Handler splashHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STOPSPLASH:

                    splash.setVisibility(View.GONE);
                    break;
            }
            super.handleMessage(msg);
        }
    };


    public String[] getPlantList() {
        List<Plant> result = null;

        DBHelper dbHelper = new DBHelper(this);
        PlantDataSource ds = new PlantDataSource(this);
        ds.open();
        result = ds.getAllPlants();
        String result1[] = new String[result.size()];
        int i = 0;
        for (Plant plant : result) {
            result1[i] = plant.getName();
            i++;
        }
        Log.d("got", result.toString());
        ds.close();
        return result1;
    }

    ;




    @Override

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.main);

        getPlantList();
        btnActTwo = (Button) findViewById(R.id.buttonConverter);
        btnActTwo.setOnClickListener(this);

        setContentView(R.layout.main);
        //получаем индентификатор ImageView с Splash картинкой
        splash = (ImageView) findViewById(R.id.splashscreen);
        Message msg = new Message();
        msg.what = STOPSPLASH;
        splashHandler.sendMessageDelayed(msg, SPLASHTIME);

        lv1 = (ListView) findViewById(R.id.lister); // Получим идентификатор ListView
        lv1.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getPlantList())); //устанавливаем массив в ListView
        lv1.setTextFilterEnabled(true);

        //Обрабатываем щелчки на элементах ListView:
        lv1.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                //Позиция элемента, по которому щелкнули
                String itemname = new Integer(position + 1).toString();
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, PhotoListActivity.class);
                Bundle b = new Bundle();
                Plant selectedPlant=getPlant(position + 1);

                b.putLong("plantId", selectedPlant.getId()); //defStrID содержит строку, которую отправим через itemname в другое Activity
               Log.d("plantId1",selectedPlant.getId()+"");
                Log.d("plantId1", "" + b.getLong("plantId"));
                b.putString("plantName",selectedPlant.getName() );
                intent.putExtras(b);
                startActivityForResult(intent, ID_PhotoListActivity);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case ID_PhotoListActivity:{
                break;
            }
            default: ;
        }
    }

    public Plant getPlant(long id) {
        DBHelper dbHelper = new DBHelper(this);
        PlantDataSource ds = new PlantDataSource(this);
        ds.open();
        Plant plant = ds.getPlant(id);
        Log.d("dash", plant.toString());
        ds.close();
        return plant;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonConverter:
                Intent intent = new Intent(this, AddNewPlant.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}