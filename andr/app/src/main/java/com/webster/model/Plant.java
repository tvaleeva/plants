package com.webster.model;

import android.app.Activity;
import android.util.Log;

import com.webster.db.DBHelper;
import com.webster.db.PlantDataSource;

import java.sql.Date;
import java.util.List;

import com.webster.model.Photo;

/**
 * Created by Admin on 16.03.2015.
 */
public class Plant extends Activity {
    private long id;
    private String name;
    private List<Photo> plantPhotos;

    public Plant(long id, String name) {
        this.name = name;
        this.id = id;
       /* DBHelper dbHelper = new DBHelper(this);
        PlantDataSource ds = new PlantDataSource(this);
        ds.open();
        ds.createPlant(this);
        Log.d("created", ds.getAllPlants().toString());
        ds.close();
        */
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Photo> getPlantPhotos() {
        return plantPhotos;
    }

    public void setPlantPhoto(Photo plantPhoto) {

        this.plantPhotos.add(plantPhoto);
    }





    @Override
    public String toString() {
        return this.getName() + " " + this.getId();
    }
}
