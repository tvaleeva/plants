package com.webster.model;

import android.net.Uri;

import java.sql.Date;

/**
 * Created by Admin on 22.03.2015.
 */
public class Photo {
    private long id;
    private long plant_id;
    private Uri path;
    private String description;
    private Date date;

    public Photo(Long id, Long plant_id, Uri path, String description) {
        this.id = id;
        this.plant_id = plant_id;
        this.path = path;
        this.description = description;
    }

    private Photo(String id, String description, Uri path, Date date) {
        this.description = description;
        this.path = path;
        this.date = date;
    }


    public long getId() {

        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Uri getPath() {
        return path;
    }

    public void setPath(Uri path) {
        this.path = path;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPlant_id() {
        return plant_id;
    }

    public void setPlant_id(long plant_id) {
        this.plant_id = plant_id;
    }
    public String getName(){
        return this.description;
    }
    @Override
    public String toString() {
        return this.getDescription() + " " + this.getPath()+ " " + this.getPlant_id()+"   end";
    }
}
