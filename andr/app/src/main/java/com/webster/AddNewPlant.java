package com.webster;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;

import com.webster.db.DBHelper;
import com.webster.db.PlantDataSource;
import com.webster.model.Plant;

/**
 * Created by Bublik on 10.03.2015.
 */
public class AddNewPlant  extends Activity{
    public long addPlant(String name) {
        Long id;
        DBHelper dbHelper = new DBHelper(this);
        PlantDataSource ds = new PlantDataSource(this);
        ds.open();
        id=ds.createPlant(name);
        Log.d("created", ds.getAllPlants().toString());
        ds.close();
        return id;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addnewplant);
        Button addPlant = (Button) findViewById(R.id.addPlant);
        addPlant.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Long id;
                EditText nameEditText = (EditText)findViewById(R.id.plantsName);
                String name=nameEditText.getText().toString();
                id=addPlant(name);
                Log.d("new plant", id + " " + name);
                Intent intent = new Intent();
                intent.setClass(AddNewPlant.this, MainActivity.class);
//                Bundle b = new Bundle();
//                b.putLong("plantId", id); //defStrID содержит строку, которую отправим через itemname в другое Activity
//                b.putString("plantName",name );
//                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }


}
